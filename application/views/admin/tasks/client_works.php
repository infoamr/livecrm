<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
               <?php echo form_open(admin_url('tasks/client_works')); ?>
              <div class="col-md-3">
                  <?php echo render_date_input('from','','From'); ?>
              </div>

              <div class="col-md-3">
                <?php echo render_date_input('to','','To'); ?>
              </div>
               
              <div class="col-md-3">
                <div class="select-placeholder">
                   <select name="client_id" id="client_id" class="selectpicker" data-live-search="true" data-width="100%">
                      <option value=""></option>
                      <?php foreach($get_all_clients->result() as $cl){ ?>
                      <option value="<?php echo $cl->userid ?>"><?php echo $cl->company ?></option>
                      <?php } ?>
                   </select>
                </div>
              </div>

               <div class="col-md-3">
                <button type="submit" class="btn btn-info"><?php echo _l('apply'); ?></button>
               </div>
              <?php echo form_close(); ?>
              </div>

              <div class="col-md-12">
                <hr class="no-mtop"/>
              </div>
            </div>

            <div class="clearfix"></div>
            <table class="table table-striped dt-table scroll-responsive" data-order-col="1" data-order-type="desc">
              <thead class="thead-dark">
              <tr>
               <th>Task #</th> 
               <th>Client Name</th>
               <th>Work Title</th>
               <th>Work Type</th>
               <!-- <th>Due Date</th> -->
               <th>Created Date</th>
               <th>Status</th>
               <th>Assigned To</th>
               <th>Priority</th>
              </tr>
              </thead>
              <tbody>
              <?php foreach($get_all_work as $c_work) { ?>
                <tr>
                <td><?php echo $c_work['task_number']; ?></tb>
                <td><?php echo $c_work['company']; ?></tb>
                <td><?php echo $c_work['name']; ?></tb>
                <td><?php echo $c_work['work_type']; ?></tb>
                <!-- <td><?php //echo $c_work['duedate']; ?></tb> -->
                <td><?php echo _d($c_work['dateadded']); ?></tb>
                <td><?php echo $c_work['status_name']; ?></tb>                  
                <?php $CI = &get_instance();
                  $assignees_ids = $CI->db->select('task_assigned.staffid, staff.firstname,staff.lastname')
                  ->where('taskid', $c_work['id'])
                  ->from(db_prefix() . 'task_assigned')
                  ->join(db_prefix() . 'staff', 'staff.staffid = task_assigned.staffid')
                  ->get()->result_array(); 

                  $names = array();
                  foreach ($assignees_ids as $aid) {
                    $names[] = $aid['firstname'].' '.$aid['lastname'];
                  }
                  $assignees = implode(", ", $names);
                ?>
                <td><?php echo $assignees; ?></td>
                <?php if ($c_work['priority'] == 1) {
                      $priority = _l('task_priority_low');
                    } else if ($c_work['priority'] == 2) {
                      $priority = _l('task_priority_medium');
                    } else if ($c_work['priority'] == 3) {
                      $priority = _l('task_priority_high');
                    } else if ($c_work['priority'] == 4) {
                      $priority = _l('task_priority_urgent');
                    } else {
                      $priority = '';
                    } ?>
                <td><?php echo $priority; ?></tb>
               </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
</body>
</html>
