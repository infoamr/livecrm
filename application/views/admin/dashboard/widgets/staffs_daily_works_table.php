<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style type="text/css">

.highcharts-figure, .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
}

#datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #000000;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
#datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
#datatable th {
  font-weight: 600;
    padding: 0.5em;
}
#datatable td, #datatable th, #datatable caption {
    padding: 0.5em;
    text-align: left;
}
#datatable thead tr, #datatable tr:nth-child(even) {
    background: #f8f8f8;
}
#datatable tr:hover {
    background: #f1f7ff;
}

</style>
<div style="font-weight: bold; text-align: center;"><?php echo date('d F Y');?></div>
<figure class="highcharts-figure">
<table id="datatable">
    <thead>
      <tr>
        <th>Staff Name</th> 
        <th>Task Number</th>
      </tr>
    </thead>
    <tbody>
      <?php $CI = &get_instance();
        $CI->db->select('id, task_number, dateadded');
        $CI->db->like('dateadded', date('Y-m-d'));
        $CI->db->from(db_prefix().'tasks');
        $todaytask = $CI->db->get()->result_array();
        //print_r($count);

        foreach($todaytask as $tt) {
            $CI = &get_instance();
            $CI->db->select('staffid, taskid');
            $CI->db->where('taskid', $tt['id']);
            $CI->db->group_by('staffid');
            $CI->db->from(db_prefix().'task_assigned');
            $result = $CI->db->get()->result();

            $CI = &get_instance();
            $CI->db->select('staffid, CONCAT(firstname, " ", lastname) as fullname');
            $CI->db->where('staffid', $result[0]->staffid);
            $sname = $CI->db->get(db_prefix() . 'staff')->result();
            echo '<tr><td>'.$sname[0]->fullname.'</td><td>'.$tt['task_number'].'</td></tr>'; 
        }
      ?>
    </tbody>
</table>

</figure>

