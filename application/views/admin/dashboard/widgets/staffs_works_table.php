<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style type="text/css">

  #container {
    height: 400px;
}

.highcharts-figure, .highcharts-data-table table {
    min-width: 310px;
    max-width: 800px;
    margin: 1em auto;
}

#datatable {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #000000;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
#datatable caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
#datatable th {
  font-weight: 600;
    padding: 0.5em;
}
#datatable td, #datatable th, #datatable caption {
    padding: 0.5em;
    text-align: left;
}
#datatable thead tr, #datatable tr:nth-child(even) {
    background: #f8f8f8;
}
#datatable tr:hover {
    background: #f1f7ff;
}

</style>

<script src="<?php echo base_url('assets/js/highcharts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/exporting.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/data.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/accessibility.js'); ?>"></script>

<figure class="highcharts-figure">
    <div id="container"></div>

    <table id="datatable">
        <thead>
          <tr>
            <th>Staff Name</th> 
            <th>Total Works Count</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $CI = &get_instance();
            $CI->db->select('staffid, CONCAT(firstname, " ", lastname) as fullname');
            $allstaffs = $CI->db->get(db_prefix() . 'staff')->result_array();

          foreach($allstaffs as $st){ 
            $CI = &get_instance();
            $CI->db->select('count(id) as number')->where('staffid', $st['staffid']);
            $count = $CI->db->get(db_prefix() . 'task_assigned')->result();
            echo '<tr><td>'.$st['fullname'].'</td><td>'.$count[0]->number.'</td></tr>';
          } ?>
        </tbody>
    </table>

</figure>

<script type="text/javascript">
  Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: 'Statistics by Staff Works'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Count'
        }
    },
    tooltip: {
        formatter: function () {
          return '<b>' + this.series.name + '</b><br/>' +
            this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});
</script>

