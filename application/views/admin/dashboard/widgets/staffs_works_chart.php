<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style type="text/css">

  .highcharts-figure, .highcharts-data-table table {
      min-width: 310px; 
      max-width: 800px;
      margin: 1em auto;
  }

  #container {
      height: 400px;
  }

  .highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
  }
  .highcharts-data-table caption {
      padding: 1em 0;
      font-size: 1.2em;
      color: #555;
  }
  .highcharts-data-table th {
    font-weight: 600;
      padding: 0.5em;
  }
  .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
      padding: 0.5em;
  }
  .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
      background: #f8f8f8;
  }
  .highcharts-data-table tr:hover {
      background: #f1f7ff;
  }
</style>

<script src="<?php echo base_url('assets/js/highcharts.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/exporting.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/export-data.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/accessibility.js'); ?>"></script>

<div class="widget" id="widget-<?php echo create_widget_id().'2'; ?>" data-name="<?php echo create_widget_id().'2'; ?>">
  <div class="row">
    <div class="col-md-12">
     <div class="panel_s">
       <div class="panel-body padding-10">
        <div class="widget-dragger"></div>
        <p class="padding-5">Statistics by Staff Work</p>
        <hr class="hr-panel-heading-dashboard">
        <body>
          <?php  
            $CI = &get_instance();
            $CI->db->select('staffid, CONCAT(firstname, " ", lastname) as fullname');
            $allstaffs = $CI->db->get(db_prefix() . 'staff')->result_array();
            $wname = array();
            foreach($allstaffs as $aw){
              $wname[] = "'".$aw['fullname']."'";
            }
            
            $inpr = array();
            $today = date('Y-m-d');
            foreach($allstaffs as $al){
              $CI->db->select('count(id) as number')
                      ->where('staffid', $al['staffid']);
                      // ->like('tasks.dateadded', $today, 'after');
              $inprocess = $CI->db->get(db_prefix() . 'task_assigned')->result();
              $inpr[] = $inprocess[0]->number;
            }

            $str = implode(', ', $wname);
            $prstr = implode(', ', $inpr);

            // echo '<pre>';
            // print_r($wname);
            // echo $str;
            // exit;
          ?>

        <figure class="highcharts-figure">
          <div style="height:750px" id="container"></div>
        </figure>
      </body>
     </div>
   </div>
 </div>
</div>
</div>


<script type="text/javascript">
  Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Staff Works'
    },
    xAxis: { 
        categories: [<?php print_r($str); ?>]
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'Total Works Count',
        data: [<?php echo $prstr; ?>]
        // data: [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
    ]
});
</script>

