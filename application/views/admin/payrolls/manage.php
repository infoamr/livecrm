<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
     <div class="col-md-12">
        <div class="panel_s">
           <div class="panel-body">
              <div class="row _buttons">
                 <div class="col-md-12">
                    <?php if(has_permission('tasks','','create')){ ?>
                    <a href="<?php echo admin_url('payroll/pay'); ?>" class="btn btn-info mright5 test pull-left display-block">New Payroll</a>
                    <a href="javascript:void(0)" class="btn btn-primary mright5 test pull-right display-block" onclick="addfedform(); return false;">New Federal Form</a>
                    <a href="javascript:void(0)" class="btn btn-primary mright5 test pull-right display-block" onclick="addstateform(); return false;">New State Form</a>
                    <?php } ?>
                 </div>
              </div>
              <hr class="hr-panel-heading hr-10" />
              <div class="row _buttons">
                 <div class="col-md-12">
                    <a href="<?php echo admin_url('payroll/paypdf'); ?>" class="btn btn-success mright5 test pull-right display-block">PDF Export</a>
                 </div>
              </div>
              <hr class="hr-panel-heading hr-10" />
              <div class="clearfix"></div>
              <table class="table-condensed dt-table scroll-responsive" data-order-col="4" data-order-type="desc">
                 <thead>
                  <tr>
                    <th>#</th>
                    <th>Company</th>
                    <th>Federal</th> 
                    <th>Due Date</th>
                    <th>Created Date</th> 
                    <th>Priority</th>
                  </tr>
                 </thead>
                 <tbody>
                  <?php foreach($fetch_data as $fd) { ?>
                  <tr>
                    <?php 
                    $outputName = '';
                    $outputName .= '<a href=" '.admin_url('payroll/viewpay/' . $fd['id']).'">'.$fd['payroll_number'].'</a><br/>';
                    if ($fd['recurring'] == 1) {
                      $outputName .= '<span class="label label-primary inline-block mtop4"> ' . _l('recurring_tasks') . '</span><br />';
                    }
                    $outputName .= '<div class="row-options"><a href="'.admin_url('payroll/editpay/' . $fd['id']).'">' . _l('edit') . '</a><span class="text-dark"> | </span><a href="' . admin_url('payroll/deletepay/' . $fd['id']) . '" class="text-danger" onclick="return ConfirmDialog();">' . _l('delete') . '</a></div>'; ?>
                    <td><?php echo $outputName; ?></td>
                    <td><?php echo $fd['company']; ?></td>

                    <?php $fedstatus = '';
                    if($fd['federal_status'] == 1) {
                      $fedstatus = '<span class="text-warning">Pending</span>';
                    }
                    if($fd['federal_status'] == 2) {
                      $fedstatus = '<span class="text-info">In Progress</span>';
                    }
                    if($fd['federal_status'] == 3) {
                      $fedstatus = '<span class="text-success">Filed</span>';
                    } ?>
                    <td><?php echo $fd['form_name'].' : '.$fedstatus; ?></td>
                    <td><?php echo _d($fd['duedate']); ?></td>
                    <td><?php echo _d($fd['dateadded']); ?></td>
                    <?php if ($fd['priority'] == 1) {
                          $priority = _l('task_priority_low');
                        } else if ($fd['priority'] == 2) {
                          $priority = _l('task_priority_medium');
                        } else if ($fd['priority'] == 3) {
                          $priority = _l('task_priority_high');
                        } else if ($fd['priority'] == 4) {
                          $priority = _l('task_priority_urgent');
                        } else {
                          $priority = '';
                        } ?>
                    <td><?php echo $priority; ?></td>
                  </tr>

                  <?php } ?>
                 </tbody>
              </table>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add_fedform_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <!-- Add New Company PopUp Start -->
         <h4 class="modal-title" id="myModalLabel">Add Federal Form</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
              <?php echo form_open(admin_url('payroll/addfedform'));  ?>
              <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
              <div class="form-group">
                <label for="name" class="control-label">Form Name</label>
                <input type="text" name="fedform" class="form-control" value="" required>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm"><?php echo _l( 'submit'); ?></button>
              </div>
              <?php echo form_close(); ?>
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Add State Form -->
<div class="modal fade" id="add_stateform_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <!-- Add New Company PopUp Start -->
         <h4 class="modal-title" id="myModalLabel">Add State Form</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
              <?php echo form_open(admin_url('payroll/addstateform'));  ?>
              <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
              <div class="form-group">
                <select id="state" name="state" data-live-search="true" data-width="100%" class="selectpicker">
                  <option value="">Select State</option>
                  <?php foreach($states_data as $std) { ?>
                  <option value="<?php echo $std['name']?>"><?php echo $std['name']?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="name" class="control-label">Form Name</label>
                <input type="text" name="form" class="form-control" value="" required>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info btn-sm"><?php echo _l( 'submit'); ?></button>
              </div>
              <?php echo form_close(); ?>
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Add State Form -->

<?php init_tail(); ?>

<script>
  function addstateform(){
    
    $('#add_stateform_model').modal({
      show: true
    });
  }
  
  function addfedform(){
    $('#add_fedform_modal').modal({
      show: true
    });
  }

  function viewstates(id){
    $('#show_statedata').modal({
      show: true
    });
  }

 function ConfirmDialog() {
    var x=confirm("Are you sure to delete record?")
    if (x) {
      return true;
    } else {
      return false;
    }
  }
</script>

</body>
</html>
