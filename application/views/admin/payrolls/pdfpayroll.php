<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>PayRoll List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <div class="container">
    <h3 class="text-center">Payroll List</h3>
    <table class="table-striped" data-order-col="6" data-order-type="desc" width="100%" style="font-size:14px" >
     <thead>
      <tr style="background-color:#2e4056;color:#ffffff;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;">
        <th style="height: 25px!important">#</th>
        <th style="height: 25px!important">Company</th>
        <th style="height: 25px!important">Federal</th>
        <th style="height: 25px!important">Due Date</th>
        <th style="height: 25px!important">Created Date</th> 
        <th style="height: 25px!important">Priority</th>
      </tr>
     </thead>
     <tbody">
      <?php foreach($fetch_data as $fd) { ?>
      <tr style="vertical-align: top; ">
        <?php 
        $outputName = '';
        $outputName .= $fd['payroll_number'].'<br/>';
        if ($fd['recurring'] == 1) {
          $outputName .= '<span class="label label-primary inline-block mtop4"> ' . _l('recurring_tasks') . '</span><br />';
        } ?>
        <td style="padding: 10px;padding-top:0px;  margin:10px"><?php echo $outputName; ?></td>
        <td style="padding: 10px;padding-top:0px;  margin:10px"><?php echo $fd['company']; ?></td>

        <?php $fedstatus = '';
        if($fd['federal_status'] == 1) {
          $fedstatus = '<span class="text-warning">Pending</span>';
        }
        if($fd['federal_status'] == 2) {
          $fedstatus = '<span class="text-info">In Progress</span>';
        }
        if($fd['federal_status'] == 3) {
          $fedstatus = '<span class="text-success">Filed</span>';
        } ?>
        <td style="padding: 10px;padding-top:0px;  margin:10px">
          <?php echo $fd['form_name'].': <br/>'.$fedstatus; ?></td>
        <td style="padding: 10px;padding-top:0px;  margin:10px"><?php echo _d($fd['duedate']); ?></td>
        <td style="padding: 10px;padding-top:0px;  margin:10px"><?php echo _d($fd['dateadded']); ?></td>
        <?php if ($fd['priority'] == 1) {
              $priority = _l('task_priority_low');
            } else if ($fd['priority'] == 2) {
              $priority = _l('task_priority_medium');
            } else if ($fd['priority'] == 3) {
              $priority = _l('task_priority_high');
            } else if ($fd['priority'] == 4) {
              $priority = _l('task_priority_urgent');
            } else {
              $priority = '';
            } ?>
        <td style="padding: 10px;padding-top:0px;  margin:10px"><?php echo $priority; ?></td>
      </tr>

      <?php } ?>
     </tbody>
    </table>
  </div>

</body>
</html>
