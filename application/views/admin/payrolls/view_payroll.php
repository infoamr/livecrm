<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
          <div class="col-md-12">
            <div class="panel_s">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4"><h4>Payroll Details (<?php echo $fetch_data[0]['payroll_number'] ?>)</h4>
                    </div>
                    <div class="col-md-8">
                      <a href="<?php echo admin_url('payroll/paypdfdetail/' . $fetch_data[0]['id']); ?>"class="btn btn-success mright5 test pull-right display-block">PDF Export</a>
                    </div>
                  </div>
                </div>                
                <div class="row">
                  <div class="col-md-12" style="background-color: #f9fafc; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-6">
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; font-size: 13px; color:#03A9F4;">Company</h5>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['company'] ?></p>
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Federal Form</h5>
                        <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['form_name'] ?></p>
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Form Status</h5>
                        <?php 
                           if($fetch_data[0]['federal_status'] == 1) {
                              $fedstatus = 'Pending';
                            }
                            if($fetch_data[0]['federal_status'] == 2) {
                              $fedstatus = 'In Progress';
                            }
                            if($fetch_data[0]['federal_status'] == 3) {
                              $fedstatus = 'Filed';
                            } ?>
                          <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px">
                          <?php echo $fedstatus; ?></p>
                      </div>
                    </div>
   
                    
                    <div class="col-md-6">
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Priority</h5>
                        <?php 
                           if($fetch_data[0]['priority'] == 1) {
                              $priority = 'Low';
                            }
                            if($fetch_data[0]['priority'] == 2) {
                              $priority = 'Medium';
                            }
                            if($fetch_data[0]['priority'] == 3) {
                              $priority = 'High';
                            }
                            if($fetch_data[0]['priority'] == 4) {
                              $priority = 'Urgent';
                            } ?>
                          <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo $priority; ?></p>
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 class="col-md-4 mt-0" style="font-weight: bold; color:#03A9F4;">Assigned To</h5>
                       <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['firstname'].' '.$fetch_data[0]['lastname'] ?></p>
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 style="font-weight: bold; color:#03A9F4;" class="col-md-4 mt-0">Start Date</h5>
                       <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php $stdate = date("d F Y", strtotime($fetch_data[0]['startdate'])); echo $stdate; ?></p>
                      </div>
                      <div class="form-group col-md-12" style="margin: 0px; padding: 0px">
                        <h5 style="font-weight: bold; color:#03A9F4;" class="col-md-4 mt-0">Due Date</h5>
                       <p class="col-md-8 mt-0" style="font-weight: normal; font-size: 13px"><?php $ddate = date("d F Y", strtotime($fetch_data[0]['duedate'])); echo $ddate; ?></p>
                      </div>

                    </div>
                  </div>
                  <hr style="height:2px;border-width:0;color:gray;background-color:gray">

                  

                  <!-- Table Starts-->
                  <div class="col-md-12" >
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">State</h5></th>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">Form</h5></th>
                            <th class="text-center"><h5 style="font-weight: bold; color:#03A9F4;">Status</h5></th>
                          </tr>
                        </thead>
                        <tbody id="tbody">
                        <?php //print_r($states);
                          $allstates = json_decode($states[0]['state_forms_ids']);
                          $starr = explode(",", $allstates[0]);
                          foreach($starr as $st) { 
                           $CI = &get_instance();
                           $stateform = $CI->db->select('*')->where('id', $st['id'])->get(db_prefix() . 'state_forms')->result();
                            ?>
                          <tr>
                            <td class="text-center"><?php echo $stateform[0]->state; ?></td>
                            <td class="text-center"><?php echo $stateform[0]->form; ?></td>
                            <td class="text-center"><?php echo $stateform[0]->state_type; ?></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                  </div>
                  <!-- Table Ends -->

                  <?php if(!empty($reminder)) { ?>
                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-4" >
                       <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;"><?php echo _l('reminders').' ('._l('set_reminder_date').' )'; ?></h5>
                       <p style="font-weight: normal; font-size: 13px"><?php $rmd = date("d F Y", strtotime($reminder[0]['date'])); echo $rmd; ?></p>
                    </div>

                    <div class="col-md-4" >
                       <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Reminder to</h5>
                       <p style="font-weight: normal; font-size: 13px"><?php echo $reminder[0]['email'] ?></p>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                          <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Description</h5>
                          <p style="font-size: 13px"><?php echo $reminder[0]['description'] ?>
                          </p>
                        </div>
                    </div>
                  </div>
                  <?php } ?>

                  <?php if ($fetch_data[0]['recurring'] == 1) { ?>
                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                    <div class="col-md-3">
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Recurring Type</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['recurring_type'] ?></p>
                      </div>
                    </div>

                    <div class="col-md-3">  
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Count</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['repeat_every'] ?></p>
                      </div>
                    </div>
                  
                    <div class="col-md-3"> 
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Every</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['repeat_every'] ?></p>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Cycles</h5>
                        <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['cycles'] ?></p>
                      </div>
                    </div>
                  </div>
                  <?php } ?>

                  <div class="col-md-12" style="background-color: #f9fafc; margin-top: 5px; padding-top: 20px; padding-bottom: 10px">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-group">
                            <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Followers</h5>
                            <?php if(!empty($followers)) { ?>
                              <ul class="nav navbar-nav">
                              <?php foreach($followers as $data) { ?>
                              <li class="list-group-item" style="width: 240px"><?php echo $data['firstname'].' '.$data['lastname'] ?></li>
                               <?php } ?>
                              </ul>
                            <?php } 
                            else { ?>
                              <p style="font-weight: normal; font-size: 13px">No Followers</p>
                           <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="form-group">
                            <h5 class="mbot15" style="font-weight: bold; color:#03A9F4;">Remarks</h5>
                            <p style="font-weight: normal; font-size: 13px"><?php echo $fetch_data[0]['remarks'] ?></p>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="btn-bottom-toolbar btn-toolbar-container-out text-right custom_width">
            <a class="btn btn-info" href="<?php echo admin_url('payroll'); ?>">Back</a>
          </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
</body>
</html>
