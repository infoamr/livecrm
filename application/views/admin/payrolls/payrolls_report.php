<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s">
               <div class="panel-body">
                  <div class="clearfix"></div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo form_open(admin_url('payroll/search_payreport')); ?>
                     <div class="col-md-2">
                        <?php echo render_date_input('from','','From'); ?>
                     </div>
                     <div class="col-md-2">
                        <?php echo render_date_input('to','','To'); ?>
                     </div>

                     <?php //if(isset($view_all)){ ?>
                     <div class="col-md-3">
                        <div class="select-placeholder">
                           <select name="assigned_to" id="assigned_to" class="selectpicker" data-live-search="true" data-width="100%">
                              <option value=""><?php echo _l('all_staff_members'); ?></option>
                              <?php foreach($staff as $st){ ?>
                              <option value="<?php echo $st['staffid']; ?>"><?php echo $st['full_name']; ?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                     <?php //} ?>

                      <div class="col-md-3">
                        <div class="select-placeholder">
                          <select name="client_id" class="selectpicker" id="client_id" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                             <option value="">All Customers</option>
                             <?php foreach($client_data->result() as $cd) { ?>
                           <option value="<?php echo $cd->userid ?>"><?php echo $cd->company?></option>
                           <?php } ?>
                          </select>
                        </div>
                      </div>
                     
                     <div class="col-md-2">
                      <button type="submit" class="btn btn-info pull-left"><?php echo _l('apply'); ?></button>
                     </div>
                    <?php echo form_close(); ?>
                    </div>

                    <div class="col-md-12">
                      <hr class="no-mtop"/>
                    </div>

                  </div>
                  <div class="clearfix"></div>
                  <table class="table dt-table scroll-responsive" data-order-col="4" data-order-type="desc">
                     <thead>
                       <tr>
                          <th>#Number</th>
                          <th>Company</th>
                          <th>Assigned To</th>
                          <th>Due Date</th>
                          <th>Created Date</th>
                          <th>Priority</th>
                       </tr>
                     </thead>
                     <tbody>
                        <?php foreach($fetch_data as $fd) { ?>
                        <tr>
                          <?php 
                          $outputName = '';
                          $outputName .= '<a href=" '.admin_url('payroll/viewpay/' . $fd['id']).'">'.$fd['payroll_number'].'</a><br/>';
                          if ($fd['recurring'] == 1) {
                            $outputName .= '<span class="label label-primary inline-block mtop4"> ' . _l('recurring_tasks') . '</span><br />';
                          } ?>
                          <td><?php echo $outputName; ?></td>
                          <td><?php echo $fd['company']; ?></td>
                          <!-- <td><?php //echo $fd['federal_form']; ?></td> -->
                          <td><?php echo $fd['firstname'].' '.$fd['lastname']; ?></td>
                          <?php $CI = &get_instance();
                          $paystates = $CI->db->select('*')->where('payroll_id', $fd['id'])->limit(3)->get(db_prefix() . 'payroll_states')->result_array();

                          $allstates = $CI->db->select('*')->where('payroll_id', $fd['id'])->get(db_prefix() . 'payroll_states')->result_array();
                          
                          $statesdata = '';
                          $allstatesdata = '';

                          foreach($allstates as $als) {
                            $allstatesdata .= '<b>'.$als['state'].'</b><br/>';
                            if($als['type_withhold'] != 0) { 
                            $with = ($als['withhold_status']==1) ? "Pending" : (($als['withhold_status']==2)  ? "No need to file" : "Completed"); 
                            $allstatesdata .= '<span>Withhold</span> - <span class="text-info">'.$with.'</span><br/>';
                            }
                            if($als['type_unemp'] != 0) {
                            $unem = ($als['unemp_status']==1) ? "Pending" : (($als['unemp_status']==2)  ? "No need to file" : "Completed");
                            $allstatesdata .= '<span>Unemployment</span>- <span class="text-danger">'.$unem.'</span><br/>';
                            }
                            if($als['type_vat'] != 0) {
                            $vat = ($als['vat_status']==1) ? "Pending" : (($als['vat_status']==2)  ? "No need to file" : "Completed");
                            $allstatesdata .= '<span>VAT</span> - <span class="text-warning">'.$vat.'</span><br/>';
                            }
                            $allstatesdata .= '<br/>';
                          }

                          foreach($paystates as $ps) {
                            $statesdata .= '<b>'.$ps['state'].'</b><br/>';
                            if($ps['type_withhold'] != 0) { 
                            $withdst = ($ps['withhold_status']==1) ? "Pending" : (($ps['withhold_status']==2)  ? "No need to file" : "Completed"); 
                            $statesdata .= '<span>Withhold</span> - <span class="text-info">'.$withdst.'</span><br/>';
                            }
                            if($ps['type_unemp'] != 0) {
                            $unst = ($ps['unemp_status']==1) ? "Pending" : (($ps['unemp_status']==2)  ? "No need to file" : "Completed");
                            $statesdata .= '<span>Unemployment</span>- <span class="text-danger">'.$unst.'</span><br/>';
                            }
                            if($ps['type_vat'] != 0) {
                            $vatst = ($ps['vat_status']==1) ? "Pending" : (($ps['vat_status']==2)  ? "No need to file" : "Completed");
                            $statesdata .= '<span>VAT</span> - <span class="text-warning">'.$vatst.'</span><br/>';
                            }
                            $statesdata .= '<br/>';
                          } ?>
                          <?php if ($fd['priority'] == 1) {
                                $priority = _l('task_priority_low');
                              } else if ($fd['priority'] == 2) {
                                $priority = _l('task_priority_medium');
                              } else if ($fd['priority'] == 3) {
                                $priority = _l('task_priority_high');
                              } else if ($fd['priority'] == 4) {
                                $priority = _l('task_priority_urgent');
                              } else {
                                $priority = '';
                              }
                          ?>
                          <td><?php echo _d($fd['duedate']); ?></td>
                          <td><?php echo _d($fd['dateadded']); ?></td>
                          <td><?php echo $priority; ?></td>
                        </tr>

                      <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   var staff_member_select = $('select[name="staff_id"]');
   $(function() {

    init_ajax_projects_search();
    var ctx = document.getElementById("timesheetsChart");
    var chartOptions = {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: '',
          data: [],
          backgroundColor: [],
          borderColor: [],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: function(tooltipItems, data) {
              return decimalToHM(tooltipItems.yLabel);
            }
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              min: 0,
              userCallback: function(label, index, labels) {
                return decimalToHM(label);
              },
            }
          }]
        },
      }
    };

    var timesheetsTable = $('.table-timesheets-report');
    $('#apply_filters_timesheets').on('click', function(e) {
      e.preventDefault();
      timesheetsTable.DataTable().ajax.reload();
    });

    $('body').on('change','#group_by_task',function(){
      var tApi = timesheetsTable.DataTable();
      var visible = $(this).prop('checked') == false;
      var tEndTimeIndex = $('.t-end-time').index();
      var tStartTimeIndex = $('.t-start-time').index();
      if(tEndTimeIndex == -1 && tStartTimeIndex == -1) {
        tStartTimeIndex = $(this).attr('data-start-time-index');
        tEndTimeIndex = $(this).attr('data-end-time-index');
      } else {
        $(this).attr('data-start-time-index',tStartTimeIndex);
        $(this).attr('data-end-time-index',tEndTimeIndex);
      }
      tApi.column(tEndTimeIndex).visible(visible, false).columns.adjust();
      tApi.column(tStartTimeIndex).visible(visible, false).columns.adjust();
      tApi.ajax.reload();
    });

    var timesheetsChart;
    var Timesheets_ServerParams = {};
    Timesheets_ServerParams['range'] = '[name="range"]';
    Timesheets_ServerParams['period-from'] = '[name="period-from"]';
    Timesheets_ServerParams['period-to'] = '[name="period-to"]';
    Timesheets_ServerParams['staff_id'] = '[name="staff_id"]';
    Timesheets_ServerParams['project_id'] = 'select#project_id';
    Timesheets_ServerParams['clientid'] = 'select#clientid';
    Timesheets_ServerParams['group_by_task'] = '[name="group_by_task"]:checked';
    initDataTable('.table-timesheets-report', window.location.href, undefined, undefined, Timesheets_ServerParams, [<?php if(isset($view_all)){echo 3;} else {echo 2;} ?>, 'desc']);

    init_ajax_project_search_by_customer_id();

    $('#clientid').on('change', function(){
          var projectAjax = $('select#project_id');
          var clonedProjectsAjaxSearchSelect = projectAjax.html('').clone();
          var projectsWrapper = $('.projects-wrapper');
          projectAjax.selectpicker('destroy').remove();
          projectAjax = clonedProjectsAjaxSearchSelect;
          $('#project_ajax_search_wrapper').append(clonedProjectsAjaxSearchSelect);
          init_ajax_project_search_by_customer_id();
    });
</script>
</body>
</html>
