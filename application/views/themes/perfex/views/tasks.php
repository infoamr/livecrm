<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="panel_s section-heading section-projects">
   <div class="panel-body">
      <h4 class="no-margin section-text" style="color: #051e28; font-weight: 600"><?php echo $client->company; ?></h4>
   </div>
</div>
<div class="panel_s">
   <div class="panel-body">
      <div class="row mbot15">
         <div class="col-md-12">
            <h3 class="text-success projects-summary-heading no-mtop mbot15"style="color: #051e28; font-weight: 600">Work Summary</h3>
         </div>
         <?php get_template_part('tasks/task_summary'); ?>
      </div>
      <hr />
         <table class="table dt-table table-projects" data-order-col="2" data-order-type="desc">
            <thead>
               <tr style="font-weight: 600; font-size: 14px;">
                  <th class="th-project-name">Work #</th>
                  <th class="th-project-name">Work Title</th>
                  <th class="th-project-billing-type">Work Type</th>
                  <th class="th-project-start-date">Start Date</th>
                  <th class="th-project-deadline"><?php echo _l('project_deadline'); ?></th>
                  <?php
                     $custom_fields = get_custom_fields('projects',array('show_on_client_portal'=>1));
                     foreach($custom_fields as $field){ ?>
                  <th><?php echo $field['name']; ?></th>
                  <?php } ?>
                  <th><?php echo _l('project_status'); ?></th>
               </tr>
            </thead>
            <tbody>
               <?php foreach($fetch_data->result() as $data){ ?>
               <tr>
                  <td><a style="font-size:14px; font-weight:400;" href="<?php echo site_url('clients/task/'.$data->id); ?>"><?php echo $data->task_number; ?></a></td>
                  <td><?php echo $data->name; ?></td>
                  <td data-order="<?php echo $project['deadline']; ?>"><?php echo $data->work_type ?></td>
                  <td data-order="<?php echo $project['start_date']; ?>"><?php echo _d($data->startdate) ?></td>
                  <td data-order="<?php echo $project['deadline']; ?>"><?php echo _d($data->duedate) ?></td>

                  <?php foreach($custom_fields as $field){ ?>
                  <td><?php echo get_custom_field_value($project['id'],$field['id'],'projects'); ?></td>
                  <?php } ?>
                  <td style="color:green;" data-order="<?php echo $project['deadline']; ?>">
                  <?php 
                  if($data->status == 1) {
                     echo _l('task_status_1');
                  }
                  elseif($data->status == 2) {
                     echo _l('task_status_2');
                  }
                  elseif($data->status == 3) {
                     echo _l('task_status_3');
                  }
                  elseif($data->status == 4) {
                     echo _l('task_status_4');
                  }
                  elseif($data->status == 5) {
                     echo _l('task_status_5');
                  }
                  elseif($data->status == 6) {
                     echo _l('task_status_6');
                  }
                  elseif($data->status == 7) {
                     echo _l('task_status_7');
                  }
                  elseif($data->status == 8) {
                     echo _l('task_status_8');
                  }
                  elseif($data->status == 9) {
                     echo _l('task_status_9');
                  }
                  elseif($data->status == 10) {
                     echo _l('task_status_10');
                  }
                  elseif($data->status == 11) {
                     echo _l('task_status_11');
                  }
                  elseif($data->status == 12) {
                     echo _l('task_status_12');
                  }
                  elseif($data->status == 13) {
                     echo _l('task_status_13');
                  }
                  elseif($data->status == 14) {
                     echo _l('task_status_14');
                  }
                  elseif($data->status == 15) {
                     echo _l('task_status_15');
                  }?>
                  </td>
               </tr>
               <?php } ?>
            </tbody>
         </table>
   </div>
</div>
