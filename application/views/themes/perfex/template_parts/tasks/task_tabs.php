<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<ul class="nav nav-tabs no-margin" role="tablist">

    <li role="presentation" class="active project_tab_overview">
        <a data-group="project_overview" href="<?php echo site_url('clients/task/'.$task->id.'?group=task_overview'); ?>" role="tab"><i class="fa fa-th" aria-hidden="true"></i> <?php echo _l('task_overview'); ?></a>
    </li>

    
    <!-- <li role="presentation" class="project_tab_tasks">
        <a data-group="project_tasks" href="<?php //echo site_url('clients/task/'.$task->id.'?group=task_tasks'); ?>" role="tab"><i class="fa fa-check-circle" aria-hidden="true"></i>
         <?php //echo _l('tasks'); ?></a>
    </li>
     -->

    
    <!-- <li role="presentation" class="project_tab_timesheets">
        <a data-group="project_timesheets" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_timesheets'); ?>" role="tab"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php //echo _l('project_timesheets'); ?></a>
    </li> -->
    

    
    <!-- <li role="presentation" class="project_tab_milestones">
        <a data-group="project_milestones" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_milestones'); ?>" role="tab"><i class="fa fa-rocket" aria-hidden="true"></i> <?php //echo _l('project_milestones'); ?></a>
    </li> -->
    
    <li role="presentation" class="project_tab_discussions">
        <a data-group="project_discussions" href="<?php echo site_url('clients/task/'.$task->id.'?group=task_dis'); ?>" role="tab"><i class="fa fa-commenting" aria-hidden="true"></i> <?php echo _l('task_discussions'); ?></a>
    </li>

    <li role="presentation" class="project_tab_files">
        <a data-group="project_files" href="<?php echo site_url('clients/task/'.$task->id.'?group=task_files'); ?>" role="tab"><i class="fa fa-files-o" aria-hidden="true"></i> &nbsp;My Documents</a>
    </li>

    <!-- <li role="presentation" class="project_tab_discussions">
        <a data-group="project_discussions" href="<?php //echo site_url('clients/task/'.$task->id.'?group=task_discussions'); ?>" role="tab"><i class="fa fa-commenting" aria-hidden="true"></i> <?php //echo _l('task_discussions'); ?></a>
    </li> -->

    

    
    <!-- <li role="presentation" class="project_tab_gantt">
        <a data-group="project_gantt" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_gantt'); ?>" role="tab"><i class="fa fa-align-left" aria-hidden="true"></i> <?php //echo _l('project_gant'); ?></a>
    </li> -->
    

    
    <!-- <li role="presentation" class="project_tab_tickets">
        <a data-group="project_tickets" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_tickets'); ?>" role="tab"><i class="fa fa-life-ring" aria-hidden="true"></i> <?php //echo _l('project_tickets'); ?></a>
    </li> -->
    

    
    <!-- <li role="presentation" class="project_tab_contracts">
        <a data-group="project_contracts" href="<?php //echo site_url('clients/task/'.$task->id.'?group=task_contracts'); ?>" role="tab"><i class="fa fa-life-ring" aria-hidden="true"></i> <?php //echo _l('task_contracts'); ?></a>
    </li> -->
    

    
    <!-- <li role="presentation" class="project_tab_estimates">
        <a data-group="project_estimates" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_estimates'); ?>" role="tab"><i class="fa fa-sun-o" aria-hidden="true"></i> <?php //echo _l('estimates'); ?></a>
    </li> -->
    

    
    <!-- <li role="presentation" class="project_tab_invoices">
        <a data-group="project_invoices" href="<?php //echo site_url('clients/project/'.$project->id.'?group=project_invoices'); ?>" role="tab"><i class="fa fa-sun-o" aria-hidden="true"></i> <?php //echo _l('project_invoices'); ?></a>
    </li> -->
    

    
    <li role="presentation" class="project_tab_activity">
        <a data-group="project_activity" href="<?php echo site_url('clients/task/'.$task->id.'?group=task_activity'); ?>" role="tab"><i class="fa fa-exclamation" aria-hidden="true"></i> <?php echo _l('task_activity'); ?></a>
    </li>
    

</ul>
