<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends App_Controller
{
    public function index($key = '')
    {
        update_option('cron_has_run_from_cli', 1);

        if (defined('APP_CRON_KEY') && (APP_CRON_KEY != $key)) {
            header('HTTP/1.0 401 Unauthorized');
            die('Passed cron job key is not correct. The cron job key should be the same like the one defined in APP_CRON_KEY constant.');
        }

        $last_cron_run                  = get_option('last_cron_run');
        $seconds = hooks()->apply_filters('cron_functions_execute_seconds', 300);

        if ($last_cron_run == '' || (time() > ($last_cron_run + $seconds))) {
            $this->load->model('cron_model');
            $this->cron_model->run();
        }
    }
    
    public function newjob()
    {
        exit;

        $tnum = 'PR'.mt_rand(10000000,99999999);
        $insert['task_number'] = $tnum;
        $insert['name'] = 'Payroll_Demo_Test';
        $insert['work_type'] = 'Payroll';
        $insert['client_id'] = 3612;
        $insert['description'] = 'Company Name - Payroll Update '; 
        $insert['priority'] = 3;
        $insert['dateadded'] = date("Y-m-d h:i:s"); // 14th of every month
        $insert['startdate'] = date("Y-m-d"); // same as dateadded
        $day = new DateTime('last day of this month');
        // $d = new DateTime('first day of next month');
        $insert['duedate'] = $day->format('Y-m-d'); // 1st of next month
        $insert['addedfrom'] = 106;
        $insert['status'] = 1;
        echo '<pre>'; print_r($insert);

        // $this->db->insert(db_prefix() . 'tasks', $insert);
        // $lastid = $this->db->insert_id();
        // echo $lastid; die;

        $staffs = $this->db->select('staffid, email')->where('group_id',1)->get(db_prefix() . 'staff')->result_array();
        //  print_r($staffs); exit;

            // foreach ($staffs as $st) {
            //     $add['staffid'] = $st['staffid'];
            //     $add['taskid'] = $lastid;
            //     $add['assigned_from'] = 106;
            //     $this->db->insert(db_prefix() . 'task_assigned', $add);
            //     send_mail_template('task_assigned_to_staff', $st['email'], $st['staffid'], $lastid);
            // }
            // add reminder email
    }
    
}
