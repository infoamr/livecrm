<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Staff_groups_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new staff group
     * @param array $data $_POST data
     */
    public function add_group($data)
    {
        $this->db->insert(db_prefix().'staffs_groups', $data);

        $insert_id = $this->db->insert_id();

        if ($insert_id) {
            log_activity('New Staff Group Created [ID:' . $insert_id . ', Name:' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }

    /**
    * Get staff groups where staff belongs
    * @param  mixed $id staff id
    * @return array
    */
    public function get_staff_groups($id)
    {
        $this->db->where('staff_id', $id);

        return $this->db->get(db_prefix().'staff_groups')->result_array();
    }

    /**
     * Get all staff groups
     * @param  string $id
     * @return mixed
     */
    public function get_groups($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix().'staffs_groups')->row();
        }
        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix().'staffs_groups')->result_array();
    }

    /**
     * Edit staff group
     * @param  array $data $_POST data
     * @return boolean
     */
    public function edit_group($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update(db_prefix().'staffs_groups', [
            'name' => $data['name'],
        ]);
        if ($this->db->affected_rows() > 0) {
            log_activity('Staff Group Updated [ID:' . $data['id'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Delete staff group
     * @param  mixed $id group id
     * @return boolean
     */
    public function delete_group($id)
    {
        $this->db->where('id', $id);
        $this->db->delete(db_prefix().'staffs_groups');
        if ($this->db->affected_rows() > 0) {
            $this->db->where('groupid', $id);
            $this->db->delete(db_prefix().'staff_groups');

            // hooks()->do_action('customer_group_deleted', $id);

            log_activity('Staff Group Deleted [ID:' . $id . ']');

            return true;
        }

        return false;
    }
}
