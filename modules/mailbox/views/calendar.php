<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper" class="customer_profile">
   <div class="content">
      <div class="row">
         <div class="col-md">
            <div class="btn-bottom-toolbar btn-toolbar-container-out text-right">
               <a class="btn btn-info" href="<?php echo admin_url('mailbox/newmailbox'); ?>">Back</a>
            </div>
            <div class="panel_s">
               <div class="panel-body">
                     <div class="tab-content">
                           <h4 class="customer-profile-group-heading"><?php echo 'Calendar'; ?></h4>
                           <div class="row">
                              <div class="additional"></div>
                              <div class="col-md-12">
                                <div>
                                  <?php //echo $usid; ?>
                                </div>
                                <div>
                                  <?php //echo $usmail; ?>
                                </div>
                                <!-- http://server/exchange/user/?cmd=contents&fpath=inbox -->
                                
                                <iframe height="750" width="100%" src="https://outlook.office365.com/owa/calendar/c6b3842ecf4342cd85ad15475953a47b@RamAssociates.us/d7bc48a604c046889e490c889e06dc8c17424437155063699251/calendar.html"></iframe>
                              </div>
                           </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</body>
</html>
