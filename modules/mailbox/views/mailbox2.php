<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <?php $currentURL = current_url(); ?>
            <?php //echo 'errcode '.$errorcode.'<br/>'.'token ' .$_SESSION["access_token"]; ?>
            <?php if($errorcode == 401 || $errorcode == 'TokenExpired' || 
                    empty($_SESSION["access_token"])) { ?>
                <div class="col-md-3">
                    <div class="panel_s mbot5">
                        <div class="">
                        <a href="<?php echo accessUrl;?>" class="btn btn-info display-block">Connect</a>
                        </div>
                    </div>
                </div>
            <?php } else { ?>  

                <div class="col-md-3">            
                    <div class="panel_s mbot5">
                        <div class="">
                            <a href="<?php echo admin_url().'mailbox/compose'?>" class="btn btn-info display-block">
                                <i class="fa fa-edit"></i>
                                <?php echo _l('mailbox_compose');?>
                            </a>
                        </div>
                    </div>

                    <ul class="nav navbar-pills navbar-pills-flat nav-tabs nav-stacked customer-tabs" role="tablist">
                        <li class="mail_tab_myinbox" style="<?php if(strpos($currentURL, 'myinbox') !== false) {echo 'background-color:#28b8da; ';} ?>">
                            <a data-group="inbox" href="<?php echo admin_url('mailbox/myinbox'); ?>">
                                <i class="fa fa-inbox menu-icon" aria-hidden="true"></i>
                                <?php echo _l('mailbox_inbox'); ?>
                            </a>
                        </li>
                        <li class="mail_tab_sent" style="<?php if(strpos($currentURL, 'sent') !== false) {echo 'background-color:#28b8da; ';} ?>">
                            <a data-group="sent" href="<?php echo admin_url('mailbox/sentitems'); ?>">
                                <i class="fa fa-envelope-o menu-icon" aria-hidden="true"></i>
                                <?php echo _l('mailbox_sent_items'); ?>
                            </a>
                        </li>
                        <li class="mail_tab_draft" style="<?php if(strpos($currentURL, 'draft') !== false) {echo 'background-color:#28b8da; ';} ?>">
                            <a data-group="draft" href="<?php echo admin_url('mailbox/drafts'); ?>">
                                <i class="fa fa-file-o menu-icon" aria-hidden="true"></i>
                                <?php echo _l('mailbox_draft'); ?>
                            </a>
                        </li>
                        <li class="mail_tab_trash" style="<?php if(strpos($currentURL, 'trash') !== false) {echo 'background-color:#28b8da; ';} ?>">
                            <a data-group="trash" href="<?php echo admin_url('mailbox/trash'); ?>">
                                <i class="fa fa-trash-o menu-icon" aria-hidden="true"></i>
                                <?php echo _l('mailbox_trash'); ?>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-9">
                    <div class="panel_s">
                        <div class="panel-body">
                            <div class="tab-content">
                                <h4 class="customer-profile-group-heading">                            
                                    <?php if(strpos($currentURL, 'myinbox') !== false) {
                                        echo _l('mailbox_inbox');
                                    }
                                    if(strpos($currentURL, 'sent') !== false) {
                                        echo _l('mailbox_sent_items');
                                    } 
                                    if(strpos($currentURL, 'draft') !== false) {
                                        echo _l('mailbox_draft');
                                    }
                                    if(strpos($currentURL, 'trash') !== false) {
                                        echo _l('mailbox_trash');
                                    }
                                    if(strpos($currentURL, 'compose') !== false) {
                                        echo 'Compose New Mail';
                                        $this->load->view('mailbox/mailbox_compose'); 
                                    }
                                    if(strpos($currentURL, 'maild') !== false) {
                                        $this->load->view('mailbox/mailbox_detail');
                                    }
                                    if(strpos($currentURL, 'reply') !== false && strpos($currentURL, 'forward') == false) {
                                        echo 'Reply';
                                        $this->load->view('mailbox/mailbox_reply');
                                    }
                                    if(strpos($currentURL, 'forward') !== false && strpos($currentURL, 'reply') !== false) {
                                        echo 'Forward';
                                        $this->load->view('mailbox/mailbox_reply');
                                    }
                                    ?>                                    
                                </h4>
                                <?php if(strpos($currentURL,'compose') || strpos($currentURL,'maild') || strpos($currentURL,'reply') !== false) {
                                }
                                else { ?>
                                <table class="dt-table scroll-responsive" data-order-col="3" data-order-type="desc">
                                    <thead>
                                      <tr>
                                        <th>From</th>
                                        <th>Subject</th>
                                        <th>Date</th>
                                        <th style="display: none;">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody class="mai">
                                      <?php foreach($value as $mail) 
                                        {
                                            //$newdate = date('d-m-Y h:i', strtotime($mail["ReceivedDateTime"]));
                                            //$newdate = date("d-F-y h:i", strtotime($newdate));
                                            $mUrl = admin_url('mailbox/maild/').$mail["Id"];
                                            echo "<tr><td><a href=".$mUrl.">".$mail["From"]["EmailAddress"]["Address"]."</a></td>";
                                            echo "<td><a href=".$mUrl.">".$mail["Subject"]."</a></td>";
                                            echo "<td><a href=".$mUrl.">"._dt($mail["ReceivedDateTime"])."</a></td><td style='display: none;'></td></tr>";
                                        } ?>
                                    </tbody>
                                </table>
                                <!--<div style="float: right"><?php //echo $prevLink;?></div>-->
                                <?php } ?> 
                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script type="text/javascript">
	"use strict";

    $(function(){
        var webmailTableNotSortable = [0];
        initDataTable('.table-mailbox', admin_url + 'mailbox/table/<?php echo $group;?>', 'undefined', webmailTableNotSortable, 'undefined', [2, 'desc']);
    });
</script>
</body>
</html>