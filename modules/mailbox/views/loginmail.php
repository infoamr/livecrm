<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper" class="customer_profile">
   <div class="content">
      <div class="row">
         <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
            <div class="panel_s">
               <div class="panel-body">
                   <h4 class="customer-profile-group-heading">Mailbox</h4>
                   <div class="row">
                        <?php //$this->load->view('authentication/includes/alerts'); ?>
                        <?php //echo form_open($this->uri->uri_string()); ?>
                        <?php echo form_open(admin_url('mailbox/officepwd')); ?>
                        <?php echo validation_errors('<div class="alert alert-danger text-center">', '</div>'); ?>
                        <?php hooks()->do_action('after_admin_login_form_start'); ?>
                        <div class="form-group">
                          <label for="email" class="control-label"><?php echo _l('admin_auth_login_email'); ?></label>
                          <input type="email" class="form-control" value="<?php echo $member->email; ?>" readonly>
                        </div>
                        <div class="form-group">
                          <label for="office_password" class="control-label"><?php echo _l('admin_auth_login_password'); ?></label>
                          <input type="password" id="office_password" name="office_password" class="form-control">
                        </div>
                        <!-- <div class="">
                          <a href="<?php //echo accessUrl;?>" class="btn btn-info display-block">Connect</a>
                        </div> -->
                        <div class="form-group">
                          <button type="submit" class="btn btn-info btn-block"><?php echo _l('admin_auth_login_button'); ?></button>
                        </div>
                        <?php hooks()->do_action('before_admin_login_form_close'); ?>
                        <?php echo form_close(); ?>
                   </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</body>
</html>
